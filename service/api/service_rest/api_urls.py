from django.urls import path

from .views import (
    api_technicians,
    api_appointments,
    api_technician,
    api_search_vin,
    api_appointment,
    api_cancel_appointment,
    api_finish_appointment,
)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:id>/", api_appointment, name="api_appointment"),
    path("appointments/search/<str:id>/", api_search_vin, name="api_search_vin"),
    path("technicians/<int:id>/", api_technician, name="api_appointment"),
    path("appointments/<int:id>/cancel/", api_cancel_appointment, name="api_cancel_appointment"),
    path("appointments/<int:id>/finish/", api_finish_appointment, name="api_finish_appointment"),
]
