from common.json import ModelEncoder
from .models import Technician, Appointment


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date",
        "time",
        "reason",
        "vin",
        "vip_status",
        "customer",
        "technician",
        "status",
        "id",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date",
        "time",
        "reason",
        "vin",
        "vip_status",
        "customer",
        "technician",
        "status",
        "id",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }
