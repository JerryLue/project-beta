from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return f"{self.vin}"


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)


class Appointment(models.Model):
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.TextField()
    vip_status = models.BooleanField(default=False)
    status = models.CharField(max_length=20, default="active")
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician, related_name="appointment", on_delete=models.CASCADE
    )

    def canceled(self):
        self.status = "canceled"
        self.save()

    def finished(self):
        self.status = "finished"
        self.save()

    def get_api_url(self):
        return reverse("api_detail_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.customer, "appointment"

    class Meta:
        ordering = ["date"]
