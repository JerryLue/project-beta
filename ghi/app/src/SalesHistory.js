import React, { useEffect, useState } from "react";


function SalesHistory(){
    const [sales, setSales] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState("")
    const [employeeID, setEmployeeID] = useState("")

	const loadSales = async () => {
		const url = "http://localhost:8090/api/sales/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setSales(data.sale);
            console.log(data.sale)
		}
	};

    useEffect(() => {
		loadSales();
	}, []);

    const loadSalespeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    };

    useEffect(() => {
        loadSalespeople();
    }, []);

    const handleSalespersonChange = async (event) => {
        event.preventDefault();
        const salespersonUrl = `http://localhost:8090/api/sales/history/${salesperson.salesperson.id}/`;
        const searchResponse = await fetch(salespersonUrl);
        if (searchResponse.ok){
            const searchData = await searchResponse.json();
            setSalesperson(searchData)
        }
    }


    return(
        <div className="px-4 py-5 my-5 text-left">
        <h1>Salesperson History</h1>
        <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
            <option value="">Choose a Salesperson...</option>
            {salespeople.map(salesperson => {
                return(
                    <option key={salesperson.first_name} value={salesperson.first_name}>{salesperson.first_name} {salesperson.last_name}</option>
                )
            })}
        </select>

        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
            {sales.filter((sale) => sale.salesperson.id ).map((sale) => {
            {sales?.map(sale => {
                return (
                    <tr key={sale.id}>
                        <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                        <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                        <td>{sale.automobile.vin}</td>
                        <td>{sale.price}</td>
                    </tr>
                );
            })}})}

            </tbody>
        </table>
    </div>
    )
}


export default SalesHistory;
