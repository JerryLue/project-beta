import React, { useEffect, useState } from "react";

function AddAutomobile() {
    const [modelList, setmodelList] = useState([]);
    const [color, setColor] = useState("");
    const [year, setYear] = useState("");
    const [vin, setVin] = useState("");
    const [model, setModel] = useState("");

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    };
    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    };
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    };
    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    };

    const modelListData = async () => {
        const modelUrl = "http://localhost:8100/api/models/";
        const response = await fetch(modelUrl);

        if (response.ok) {
            const data = await response.json();
            setmodelList(data.models);
        }
    };

    useEffect(() => {
        modelListData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;

        const appointmentsUrl = `http://localhost:8100/api/automobiles/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            setColor("");
            setYear("");
            setVin("");
            setModel("");
        }
    };
    return (
        <div className="row">
            <div>
                <h1>Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input
                            value={color}
                            onChange={handleColorChange}
                            placeholder="Color..."
                            required
                            type="text"
                            id="color"
                            name="color"
                            className="form-control"
                        />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={year}
                            onChange={handleYearChange}
                            placeholder="Year..."
                            required
                            type="text"
                            id="year"
                            name="year"
                            className="form-control"
                        />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input
                            value={vin}
                            onChange={handleVinChange}
                            placeholder="VIN..."
                            required
                            type="text"
                            id="vin"
                            name="vin"
                            className="form-control"
                        />
                        <label htmlFor="vin">Automobile VIN</label>
                    </div>
                    <div className="mb-3">
                        <select
                            value={model}
                            onChange={handleModelChange}
                            placeholder="Choose a model..."
                            required
                            id="model"
                            name="model"
                            className="form-select"
                        >
                            <option value="models">Models</option>
                            {modelList.map((model) => {
                                return (
                                    <option key={model.id} value={model.id}>
                                        {model.manufacturer.name} {model.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    );
}

export default AddAutomobile;
