import React, { useEffect, useState } from "react";

function HistoryAppointment() {
    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState("");

    const handleSearchChange = (event) => {
        const value = event.target.value;
        setSearch(value);
    };

    const loadAppointments = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointment);
        }
    };

    useEffect(() => {
        loadAppointments();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const searchUrl = `http://localhost:8080/api/appointments/search/${search}/`;
        const searchResponse = await fetch(searchUrl);
        if (searchResponse.ok) {
            const searchData = await searchResponse.json();
            setAppointments(searchData);
        }
    };

    return (
        <div className="px-4 py-5 my-5 text-left">
            <h1>Service History</h1>
            <div>
                <form>
                    <input
                        value={search}
                        onChange={handleSearchChange}
                        placeholder="Search by VIN..."
                        required
                        type="text"
                        id="vin"
                        name="vin"
                        className="form-control"
                    />
                    <button
                        className="btn-outline-danger"
                        onClick={handleSubmit}
                    >
                        Search
                    </button>
                </form>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments
                        .filter(
                            (appointment) =>
                                appointment.status === "canceled" ||
                                appointment.status === "finished"
                        )
                        .map((appointment) => {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    {appointment.vip_status === false && (
                                        <td>No</td>
                                    )}
                                    {appointment.vip_status === true && (
                                        <td>Yes</td>
                                    )}
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.time}</td>
                                    <td>
                                        {appointment.technician.first_name}{" "}
                                        {appointment.technician.last_name}
                                    </td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.status}</td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}
export default HistoryAppointment;
