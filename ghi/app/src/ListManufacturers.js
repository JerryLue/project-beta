import React, { useEffect, useState } from "react";

function Manufacturers() {
    const [manufacturers, setManufacturers] = useState([]);
    const loadManufacturers = async () => {
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    };
    useEffect(() => {
        loadManufacturers();
    }, []);

    return (
        <div className="px-4 py-5 my-5 text-left">
            <h1>Manufacturers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map((manufacturer) => {
                        return (
                            <tr key={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default Manufacturers;
