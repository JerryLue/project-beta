import React, { useEffect, useState } from "react";

function Automobiles() {
    const [autos, setAutos] = useState([]);
    const loadAutos = async () => {
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    };
    useEffect(() => {
        loadAutos();
    }, []);

    return (
        <div className="px-4 py-5 my-5 text-left">
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map((auto) => {
                        return (
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                {auto.sold === false && <td>No</td>}
                                {auto.sold === true && <td>Yes</td>}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default Automobiles;
