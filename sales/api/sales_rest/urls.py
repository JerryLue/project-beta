from django.urls import path
from .views import (
    api_salespeople,
    api_customers,
    api_sales,
    api_salesperson,
    api_customer_edit,
    api_sale_edit,
    api_search_sale
)


urlpatterns = [
    path("sales/history/<int:id>/", api_search_sale, name="api_search_sale"),
    path("sales/<int:pk>/", api_sale_edit, name="api_sale_edit"),
    path("customers/<int:pk>/", api_customer_edit, name="api_customer_edit"),
    path("salespeople/<int:pk>/", api_salesperson, name="api_salesperson"),
    path("sales/", api_sales, name="api_sales"),
    path("customers/", api_customers, name="api_customers"),
    path("salespeople/", api_salespeople, name="api_salespeople"),
]
