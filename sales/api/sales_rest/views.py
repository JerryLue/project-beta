from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Salespeople, Customer, Sale
from django.http import JsonResponse
import json
from .encoders import SalespeopleEncoder, CustomerEncoder, SaleEncoder


@require_http_methods(["GET", "POST"])
def api_salespeople(request):
    if request.method == "GET":
        salespeople = Salespeople.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespeopleEncoder,
        )
    else:
        content = json.loads(request.body)
        salespeople = Salespeople.objects.create(**content)
        return JsonResponse(
            salespeople,
            encoder=SalespeopleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_salesperson(request, pk):
    if request.method == "GET":
        try:
            model = Salespeople.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=SalespeopleEncoder,
                safe=False
            )
        except Salespeople.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = Salespeople.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=SalespeopleEncoder,
                safe=False,
            )
        except Salespeople.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            model = Salespeople.objects.get(id=pk)
            props = ["name", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(model, prop, content[prop])
            model.save()
            return JsonResponse(
                model,
                encoder=SalespeopleEncoder,
                safe=False,
            )
        except Salespeople.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer_edit(request, pk):
    if request.method == "GET":
        try:
            model = Customer.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = Customer.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            model = Customer.objects.get(id=pk)
            props = ["name", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(model, prop, content[prop])
            model.save()
            return JsonResponse(
                model,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sale = Sale.objects.all()
        return JsonResponse(
            {"sale": sale},
            encoder=SaleEncoder,
        )
    else:

        content = json.loads(request.body)
        vin = content["automobile"]
        automobile = AutomobileVO.objects.get(vin=vin)
        content["automobile"] = automobile
        salesperson = Salespeople.objects.get(
            employee_id=content["salesperson"]
        )
        content["salesperson"] = salesperson
        customer = Customer.objects.get(first_name=content["customer"])
        content["customer"] = customer
        model = Sale.objects.create(**content)
        return JsonResponse(
            model,
            encoder=SaleEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale_edit(request, pk):
    if request.method == "GET":
        try:
            model = Sale.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=SaleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = Sale.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            model = Sale.objects.get(id=pk)
            props = ["name", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(model, prop, content[prop])
            model.save()
            return JsonResponse(
                model,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET"])
def api_search_sale(request, id):
    if request.method == "GET":
        try:
            sales = Sale.objects.filter(id=id)
            return JsonResponse(
                sales,
                encoder=SalespeopleEncoder,
                safe=False
            )
        except Sale.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
