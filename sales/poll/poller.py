import requests
import django
import os
import sys
import time
import json


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()


from sales_rest.models import AutomobileVO


def get_automobile_vo():
    response = requests.get(
        "http://project-beta-inventory-api-1:8000/api/automobiles/"
        )
    content = json.loads(response.content)
    print(content)
    for automobile in content["autos"]:
        try:
            new_vo, if_created = AutomobileVO.objects.update_or_create(
                import_href=automobile["href"],
                defaults={
                    "vin": automobile["vin"]
                }
            )
            if if_created:
                print("created an AutomobileVO object:", new_vo)
            else:
                print("Updated AutomobileVO object:", new_vo)
        except Exception as e:
            print("Error: could not update or create object:", e)


def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_automobile_vo()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
